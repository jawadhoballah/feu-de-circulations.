
/* 
 * Code d'exemple pour un capteur à ultrasons HC-SR04.
 */

/* Constantes pour les broches */
const byte TRIGGER_PIN = 2; // Broche TRIGGER
const byte ECHO_PIN = 3;    // Broche ECHO
const byte R1_PIN = 4 ;    //
const byte J1_PIN = 5;    //
const byte V1_PIN = 6;    //
const byte R1P_PIN = 7;    //
const byte V1P_PIN = 8;    //
const byte R2_PIN = 9;    //
const byte J2_PIN = 10;    //
const byte V2_PIN = 11;    //
const byte R2p_PIN = 12;    //
const byte V2p_PIN = 13;    //
double t;
double vitessP;

/* Constantes pour le timeout */
const unsigned long MEASURE_TIMEOUT = 25000UL; // 25ms = ~8m à 340m/s

/* Vitesse du son dans l'air en mm/us */
const float SOUND_SPEED = 340.0 / 1000;
int c = 1;

/** Fonction setup() */
void setup() {
   pinMode(R1_PIN, OUTPUT);
   pinMode(J1_PIN, OUTPUT);
   pinMode(V1_PIN, OUTPUT);
   pinMode(R1P_PIN, OUTPUT);
   pinMode(V1P_PIN, OUTPUT);
   pinMode(R2_PIN, OUTPUT);
   pinMode(J2_PIN, OUTPUT);
   pinMode(V2_PIN, OUTPUT);
   pinMode(R2p_PIN, OUTPUT);
   pinMode(V2p_PIN, OUTPUT);
  /*    R1_PIN
      J1_PIN
      V1_PIN
      
      R1P_PIN
      V1P_PIN
      
      R2_PIN
      J2_PIN
      V2_PIN
      
      R2p_PIN
      V2p_PIN
*/
      
  //Initialise le port série */
  Serial.begin(115200);
   
   //Initialise les broches 
 pinMode(TRIGGER_PIN, OUTPUT);
  digitalWrite(TRIGGER_PIN, LOW); // La broche TRIGGER doit être à LOW au repos
  pinMode(ECHO_PIN, INPUT);
}
void loop(){
  
 
      if( c == 1){
         digitalWrite(V1_PIN,HIGH);
         digitalWrite(V1P_PIN,HIGH);
         digitalWrite(R2_PIN,HIGH);
         digitalWrite(R2p_PIN,HIGH);

          delay(5000);
          //1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER
          digitalWrite(TRIGGER_PIN, HIGH);
          //delayMicroseconds(10);
          digitalWrite(TRIGGER_PIN, LOW);
          //2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe)
          long measure = pulseIn(ECHO_PIN, HIGH, MEASURE_TIMEOUT);
          //3. Calcul la distance à partir du temps mesurs
          float distance_mm = ((measure / 2.0 * SOUND_SPEED));
          //Affiche les résultats en mm, cm et m 
          //Serial.print(F("Distance: "));
          ////Serial.print(distance_mm);
          Serial.println(measure);
          
          Serial.print(distance_mm);
          
         
          Serial.print(F("cm, "));
       

          if(distance_mm <5 );{
             

            vitessP = distance_mm /(t/2);
            t = distance_mm /  vitessP;
            Serial.println(F("le temps ajoute est : "));
            
            Serial.print(t);
            pinMode(J2_PIN, HIGH);

            delay (t);
          }
          
            
          









          

          digitalWrite(V1_PIN, LOW);
         digitalWrite(V1P_PIN, LOW);
         digitalWrite(R2_PIN, LOW);
         digitalWrite(R2p_PIN, LOW);

        c = 2;
      }
       if( c == 2){
         digitalWrite(J1_PIN,HIGH);
         digitalWrite(R1P_PIN,HIGH);
         digitalWrite(R2_PIN,HIGH);
         digitalWrite(R2p_PIN,HIGH);
         
          delay(5000);

          digitalWrite(J1_PIN, LOW);
         digitalWrite(R1P_PIN, LOW);
         digitalWrite(R2_PIN, LOW);
         digitalWrite(R2p_PIN, LOW);
  
         c = 3;
         if(false){c = 6;};
      }
       if( c == 3){
         digitalWrite(R1_PIN,HIGH);
         digitalWrite(R1P_PIN,HIGH);
         digitalWrite(V2_PIN,HIGH);
         digitalWrite(V2p_PIN,HIGH);
         
         delay(5000);
         
         digitalWrite(R1_PIN, LOW);
         digitalWrite(R1P_PIN, LOW);
         digitalWrite(V2_PIN, LOW);
         digitalWrite(V2p_PIN, LOW);

        c = 4;
      }
       if( c == 4){
         digitalWrite(R1_PIN,HIGH);
         digitalWrite(R1P_PIN,HIGH);
         digitalWrite(J2_PIN,HIGH);  
         digitalWrite(R2p_PIN,HIGH);
          
          delay(5000);

          digitalWrite(R1_PIN, LOW);
         digitalWrite(R1P_PIN, LOW);
         digitalWrite(J2_PIN, LOW);  
         digitalWrite(R2p_PIN, LOW);
  
        c = 1;
        if(false){ c = 5; };
      }
       if( c == 5){
       
         digitalWrite(V1_PIN,HIGH);
         digitalWrite(V1P_PIN,HIGH);
         digitalWrite(R2_PIN,HIGH);
         digitalWrite(R2p_PIN,HIGH);
         
         delay(5000);

         digitalWrite(V1_PIN, LOW);
         digitalWrite(V1P_PIN, LOW);
         digitalWrite(R2_PIN, LOW);
         digitalWrite(R2p_PIN, LOW);
   
        c = 2;
      }
       if( c == 6){
         digitalWrite(R1_PIN,HIGH);
         digitalWrite(R1P_PIN,HIGH);      
         digitalWrite(V2_PIN,HIGH);
         digitalWrite(V2p_PIN,HIGH); 
         
         delay(5000);
         
         digitalWrite(R1_PIN, LOW);
         digitalWrite(R1P_PIN, LOW);
         digitalWrite(V2_PIN, LOW);
         digitalWrite(V2p_PIN, LOW);

         c = 4;
      } 
  }
/** Fonction loop() */
/**void loop() {
   
   digitalWrite(R1_PIN, LOW);   //
   delay(1000000);
   digitalWrite(R1_PIN, HIGH);   //
   delay(1000);
   
   digitalWrite(J1_PIN, LOW);   //
   delay(1000);
   digitalWrite(J1_PIN, HIGH);   //
   delay(1000);

   digitalWrite(V1_PIN, LOW);   //
   delay(1000);
   digitalWrite(V1_PIN, HIGH);   //
   delay(1000);

   
  /* 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER */
 /* digitalWrite(TRIGGER_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN, LOW);
  */
  /* 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe) */
 // long measure = pulseIn(ECHO_PIN, HIGH, MEASURE_TIMEOUT);
   
  /* 3. Calcul la distance à partir du temps mesuré */
 // float distance_mm = measure / 2.0 * SOUND_SPEED;
   
  /* Affiche les résultats en mm, cm et m */
 /* Serial.print(F("Distance: "));
  Serial.print(distance_mm);
  Serial.print(F("mm ("));
  Serial.print(distance_mm / 10.0, 2);
  Serial.print(F("cm, "));
  Serial.print(distance_mm / 1000.0, 2);
  Serial.println(F("m)"));
   
  /* Délai d'attente pour éviter d'afficher trop de résultats à la seconde */
 // delay(1000);
//}*/
