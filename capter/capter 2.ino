
/* 
 * Code d'exemple pour un capteur à ultrasons HC-SR04.
 */
/* Constantes pour les broches */
const byte TRIGGER_PIN = 2; // Broche TRIGGER
const byte ECHO_PIN = 3;    // Broche ECHO

// Les pins pour les LED
const byte R1_PIN = 4 ;    //
const byte J1_PIN = 5;    //
const byte V1_PIN = 6;    //
const byte R1P_PIN = 7;    //
const byte V1P_PIN = 8;    //
const byte R2_PIN = 9;    //
const byte J2_PIN = 10;    //
const byte V2_PIN = 11;    //
const byte R2p_PIN = 12;    //
const byte V2p_PIN = 13;    //

double distance;           //
double dmax = 100;
double tempsnormal = 100;
double vitessp;
double tempsajouter;
/* Constantes pour le timeout */
const unsigned long MEASURE_TIMEOUT = 25000UL; // 25ms = ~8m à 340m/s

/* Vitesse du son dans l'air en mm/us */
const float SOUND_SPEED = 340.0 / 1000;
int c = 1;
/** Fonction setup() */
void setup() {
   pinMode(R1_PIN, OUTPUT);
   pinMode(J1_PIN, OUTPUT);
   pinMode(V1_PIN, OUTPUT);
   pinMode(R1P_PIN, OUTPUT);
   pinMode(V1P_PIN, OUTPUT);
   pinMode(R2_PIN, OUTPUT);
   pinMode(J2_PIN, OUTPUT);
   pinMode(V2_PIN, OUTPUT);
   pinMode(R2p_PIN, OUTPUT);
   pinMode(V2p_PIN, OUTPUT);
    
  /* Initialise le port série */
  Serial.begin(115200);
   
   //Initialise les broches 
   pinMode(TRIGGER_PIN, OUTPUT);
  digitalWrite(TRIGGER_PIN, LOW); // La broche TRIGGER doit être à LOW au repos
  pinMode(ECHO_PIN, INPUT);
}
void loop(){
  


        
         digitalWrite(V1_PIN,HIGH);
         digitalWrite(V1P_PIN,HIGH);
         digitalWrite(R2_PIN,HIGH);
         digitalWrite(R2p_PIN,HIGH);

          delay(5000);
          /* 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER */
          digitalWrite(TRIGGER_PIN, HIGH);
          //delayMicroseconds(10);
          digitalWrite(TRIGGER_PIN, LOW);

  /* 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe) */
  long measure = pulseIn(ECHO_PIN, HIGH, MEASURE_TIMEOUT);
   
  /* 3. Calcul la distance à partir du temps mesuré */
  float distance_mm = (measure / 2.0 * SOUND_SPEED)/(10);
   
   //Affiche les résultats en mm, cm et m
   Serial.print("La Distance captueé du pieton est: ");
  Serial.print(distance_mm);
  Serial.println(" cm");
  
  if (distance_mm > 25 && distance_mm <= 50){
    digitalWrite(J2_PIN,HIGH);
 
            
            distance = dmax - distance_mm;
            
            vitessp = distance / (tempsnormal/2);
            tempsajouter = distance_mm/vitessp;
            Serial.print("Sa vitesse est: ");
            Serial.print(vitessp);
            Serial.println("m/s, (personne a mobelité réduite.)");
            Serial.print("le temps nécessaire pour traversé  est: ");
            Serial.print((tempsajouter*150)+ 10000);
            Serial.println("ms.");
            Serial.print("Le temps ajouter est :");
            Serial.print(tempsajouter*150);
            Serial.println("ms.");

            delay(tempsajouter*150);
            
            //
            //digitalWrite(J2_PIN,LOW);
            }else{
             Serial.println("Le pieton a une vitesse normal, le temps ajouté est 0 seconde");
            }
              delay(5000);
             
              
            //delay(1000 - tempsajouter);
            

          

         digitalWrite(V1_PIN, LOW);
         digitalWrite(V1P_PIN, LOW);
         digitalWrite(R2_PIN, LOW);
         digitalWrite(R2p_PIN, LOW);


         digitalWrite(J1_PIN,HIGH);
         digitalWrite(R1P_PIN,HIGH);
         digitalWrite(R2_PIN,HIGH);
         digitalWrite(R2p_PIN,HIGH);
         
          delay(5000);

          digitalWrite(J1_PIN, LOW);
         digitalWrite(R1P_PIN, LOW);
         digitalWrite(R2_PIN, LOW);
         digitalWrite(R2p_PIN, LOW);
  
  

         digitalWrite(R1_PIN,HIGH);
         digitalWrite(R1P_PIN,HIGH);
         digitalWrite(V2_PIN,HIGH);
         digitalWrite(V2p_PIN,HIGH);
         
         delay(5000);
         
         digitalWrite(R1_PIN, LOW);
         digitalWrite(R1P_PIN, LOW);
         digitalWrite(V2_PIN, LOW);
         digitalWrite(V2p_PIN, LOW);

         digitalWrite(R1_PIN,HIGH);
         digitalWrite(R1P_PIN,HIGH);
         digitalWrite(J2_PIN,HIGH);  
         digitalWrite(R2p_PIN,HIGH);
          
          delay(5000);

          digitalWrite(R1_PIN, LOW);
         digitalWrite(R1P_PIN, LOW);
         digitalWrite(J2_PIN, LOW);  
         digitalWrite(R2p_PIN, LOW);
  
        c = 1;
        if(false){ c = 5; };
      
       if( c == 5){
       
         digitalWrite(V1_PIN,HIGH);
         digitalWrite(V1P_PIN,HIGH);
         digitalWrite(R2_PIN,HIGH);
         digitalWrite(R2p_PIN,HIGH);
         
         delay(5000);

         digitalWrite(V1_PIN, LOW);
         digitalWrite(V1P_PIN, LOW);
         digitalWrite(R2_PIN, LOW);
         digitalWrite(R2p_PIN, LOW);
   
        c = 2;
      }
       if( c == 6){
         digitalWrite(R1_PIN,HIGH);
         digitalWrite(R1P_PIN,HIGH);      
         digitalWrite(V2_PIN,HIGH);
         digitalWrite(V2p_PIN,HIGH); 
         
         delay(5000);
         
         digitalWrite(R1_PIN, LOW);
         digitalWrite(R1P_PIN, LOW);
         digitalWrite(V2_PIN, LOW);
         digitalWrite(V2p_PIN, LOW);

         c = 4;
      }
      } 
  
/** Fonction loop() */
/**void loop() {
   
   digitalWrite(R1_PIN, LOW);   //
   delay(1000000);
   digitalWrite(R1_PIN, HIGH);   //
   delay(1000);
   
   digitalWrite(J1_PIN, LOW);   //
   delay(1000);
   digitalWrite(J1_PIN, HIGH);   //
   delay(1000);

   digitalWrite(V1_PIN, LOW);   //
   delay(1000);
   digitalWrite(V1_PIN, HIGH);   //
   delay(1000);

   
  /* 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER */
 /* digitalWrite(TRIGGER_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN, LOW);
  */
  /* 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe) */
 // long measure = pulseIn(ECHO_PIN, HIGH, MEASURE_TIMEOUT);
   
  /* 3. Calcul la distance à partir du temps mesuré */
 // float distance_mm = measure / 2.0 * SOUND_SPEED;
   
  /* Affiche les résultats en mm, cm et m */
 /* Serial.print(F("Distance: "));
  Serial.print(distance_mm);
  Serial.print(F("mm ("));
  Serial.print(distance_mm / 10.0, 2);
  Serial.print(F("cm, "));
  Serial.print(distance_mm / 1000.0, 2);
  Serial.println(F("m)"));
   
  /* Délai d'attente pour éviter d'afficher trop de résultats à la seconde */
 // delay(1000);
//}*/
